﻿using SRPGStudioToolkit.Files;

namespace SRPGStudioToolkit
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //TestLanguageFile();
            //TestSaveFile();
            TestEnvironment();
        }

        public static void TestLanguageFile()
        {
            const string languageFilename = @"<YOUR_GAME_DIRECTORY>\language.dat";

            LanguageFile file = new LanguageFile();
            file.LoadFromFile(languageFilename);
        }

        public static void TestSaveFile()
        {
            const string saveFilename = @"<YOUR_GAME_DIRECTORY>\Save\save01.sav";

            SaveFile file = new SaveFile();
            file.LoadFromFile(saveFilename);
        }

        public static void TestEnvironment()
        {
            const string envFilename = @"<YOUR_GAME_DIRECTORY>\environment.evs";

            var file = new EnvironmentFile();
            file.LoadFromFile(envFilename);
        }
    }
}
