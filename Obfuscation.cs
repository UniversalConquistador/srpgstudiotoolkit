﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRPGStudioToolkit
{
    public enum ObfuscationMode
    {
        None = 0,
        Type1 = 1,
        Type2 = 2,
    }

    public static class Obfuscation
    {
        public static readonly byte[] XorKey =
        {
            0x54, 0x94, 0xC1, 0x58,
            0xF4, 0x4C, 0x92, 0x1B,
            0xAD, 0xE0, 0x9E, 0x3A,
            0x49, 0xD1, 0xC9, 0x92,
        };

        public static void ApplyXor(byte[] data, ObfuscationMode mode)
        {
            for (int i = 0; i < data.Length && i <  XorKey.Length; i++)
            {
                byte keyByte = XorKey[i];

                // ObfuscationModes tweak bytes of the key
                if (mode == ObfuscationMode.Type1 && i == 10)
                {
                    unchecked
                    {
                        keyByte = (byte)(sbyte)-114;
                    }
                }
                else if (mode == ObfuscationMode.Type2 && i == 12)
                {
                    keyByte = 73;
                }

                data[i] ^= keyByte;
            }
        }
    }
}
