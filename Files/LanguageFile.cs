﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRPGStudioToolkit.Files
{
    public class LanguageFile
    {
        public uint LanguageIndex { get; private set; }

        public List<string> Entries { get; set; } = new List<string>();

        public LanguageFile()
        {

        }

        public void LoadFromFile(string filename)
        {
            byte[] bytes = File.ReadAllBytes(filename);

            Obfuscation.ApplyXor(bytes, ObfuscationMode.None);

            byte[] decrypted = new CryptKey(CryptKey.SecretDummy, 0).DecryptBytes(bytes);

            using var reader = new BinaryReader(new MemoryStream(decrypted));
            uint entryCount = reader.ReadUInt32();
            uint languageIndex = reader.ReadUInt32();

            this.LanguageIndex = languageIndex;

            var entries = new List<string>((int)entryCount);
            for (int i = 0; i < entryCount; i++)
            {
                uint length = reader.ReadUInt32();
                string entry = System.Text.Encoding.Unicode.GetString(decrypted.AsSpan().Slice((int)reader.BaseStream.Position, (int)length));
                reader.BaseStream.Position += length;

                entries.Add(entry);
            }

            this.Entries = entries;
        }

        public void SaveToFile(string filename)
        {
            // TODO: Implement!
            throw new NotImplementedException();
        }
    }
}
