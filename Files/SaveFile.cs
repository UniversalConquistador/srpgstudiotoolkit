﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRPGStudioToolkit.Files
{
    public class SaveFile
    {
        public const uint MAGIC = 0x53564153;

        public byte[] MonolithABlob { get; set; } = new byte[0x10];
        public byte[] MenuManagerBlob { get; set; } = new byte[0x10];

        public void LoadFromFile(string filename)
        {
            byte[] bytes = File.ReadAllBytes(filename);

            Obfuscation.ApplyXor(bytes, ObfuscationMode.Type2);

            byte[] decrypted = new CryptKey(CryptKey.SecretDummy, 0).DecryptBytes(bytes);

            using var reader = new BinaryReader(new MemoryStream(decrypted));

            //
            // Header
            //

            uint magic = reader.ReadUInt32();

            if (magic != SaveFile.MAGIC)
                throw new Exception("Invalid header magic - not a save file or improper decryption?");

            this.MonolithABlob = reader.ReadBytes(0x10);
            this.MenuManagerBlob = reader.ReadBytes(0x10);

            if (reader.ReadUInt32() != 0x50D)
                throw new Exception("Expected 0x50D!");


            //
            // SerializeFuncA
            //
            uint unkA4 = reader.ReadUInt32(); // SerializeFuncA::a4
            uint unkA3 = reader.ReadUInt32(); // SerializeFuncA::a3
            uint totalPlaytimeMs = reader.ReadUInt32();
            uint mmUnk1 = reader.ReadUInt32(); // menumanager+0x04
            
            if (unkA3 != 6)
            {
                uint mmUnk2 = reader.ReadUInt32(); // menumanager+0x08
                uint mmUnk3 = reader.ReadUInt32(); // menumanager+0x0C
            }
            else
            {
                reader.ReadUInt32(); // zero
                reader.ReadUInt32(); // zero
            }

            uint mmUnk4 = reader.ReadUInt32();
            uint mmUnk5 = reader.ReadUInt32();

            // TODO: Finish!
        }
    }
}
