﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRPGStudioToolkit.Files
{
    public class EnvironmentFile
    {

        public void LoadFromFile(string filename)
        {
            byte[] bytes = File.ReadAllBytes(filename);

            Obfuscation.ApplyXor(bytes, ObfuscationMode.Type2);

            byte[] decrypted = new CryptKey(CryptKey.SecretDummy, 0).DecryptBytes(bytes);


            using var reader = new BinaryReader(new MemoryStream(decrypted));

            byte[] header = reader.ReadBytes(32); // Not sure what all the bytes here do but they don't seem interesting

            // Read menu operations
            var clearSaveDatas = ReadMenuOperation<ClearSaveData>(reader);
            var completeSwitchDatas = ReadMenuOperation<CompleteSwitchData>(reader);
            var completeGraphicsDatas = ReadMenuOperation<CompleteGraphicsData>(reader);
            var completeMediaDatas = ReadMenuOperation<CompleteMediaData>(reader);

            var executeRefDatas = ReadMenuOperation<ExecuteRefData>(reader);

            uint byteCount = reader.ReadUInt32();
            byte[] textBytes = reader.ReadBytes((int)byteCount);
            string text = Encoding.Unicode.GetString(textBytes);

            // Copy in 84 more bytes to gMenuManager
            byte[] menuManagerBytes = reader.ReadBytes(84);
            File.WriteAllBytes(filename + ".decrypted.bin", decrypted);

            // Unknown uint32
            uint unk0 = reader.ReadUInt32();

            var typeIdDatas = ReadMenuOperation<TypeIDData>(reader);



            // TEMP: Attempt to add all switches
            foreach (var data in executeRefDatas.Data)
            {
                data.Unk0 = 1;
            }

            // TEMP: Attempt to save to new file
            using var newStream = new MemoryStream();
            using var writer = new BinaryWriter(newStream);

            writer.Write(header);

            WriteMenuOperation(clearSaveDatas, writer);
            WriteMenuOperation(completeSwitchDatas, writer);
            WriteMenuOperation(completeGraphicsDatas, writer);
            WriteMenuOperation(completeMediaDatas, writer);

            WriteMenuOperation(executeRefDatas, writer);

            byte[] newTextBytes = Encoding.Unicode.GetBytes(text);
            writer.Write((uint)newTextBytes.Length);
            writer.Write(newTextBytes);

            writer.Write(menuManagerBytes);

            writer.Write(unk0);

            WriteMenuOperation(typeIdDatas, writer);

            byte[] encrypted = new CryptKey(CryptKey.SecretDummy, 0).EncryptBytes(newStream.ToArray());

            Obfuscation.ApplyXor(encrypted, ObfuscationMode.Type2);

            System.IO.File.WriteAllBytes(filename + ".unlocked", encrypted);
        }

        private MenuOperation<TData> ReadMenuOperation<TData>(BinaryReader reader) where TData : IGameData<TData>
        {
            var result = new MenuOperation<TData>();

            uint count = reader.ReadUInt32();

            for (uint i = 0; i < count; i++)
            {
                uint unk = reader.ReadUInt32();
                TData data = TData.Read(reader);
                data.Unk = unk;
                result.Data.Add(data);
            }

            return result;
        }

        private void WriteMenuOperation<TData>(MenuOperation<TData> menuOperation, BinaryWriter writer) where TData : IGameData<TData>
        {
            writer.Write((uint)menuOperation.Data.Count);

            for (int i = 0; i < menuOperation.Data.Count; i++)
            {
                writer.Write((uint)menuOperation.Data[i].Unk);
                menuOperation.Data[i].Write(writer);
            }
        }
    }

    public class MenuOperation<TData> where TData : IGameData<TData>
    {
        public List<TData> Data { get; } = new List<TData>();
    }

    public interface IGameData<TSelf> where TSelf : IGameData<TSelf>
    {
        public uint Unk { get; set; }

        public static abstract TSelf Read(BinaryReader reader);
        void Write(BinaryWriter writer);
    }

    public class ClearSaveData : IGameData<ClearSaveData>
    {
        public uint Unk { get; set; }
        public uint Unk0 { get; set; }
        public uint Unk1 { get; set; }
        public uint Unk2 { get; set; }
        public uint Unk3 { get; set; }

        public static ClearSaveData Read(BinaryReader reader)
        {
            return new ClearSaveData() { Unk0 = reader.ReadUInt32(), Unk1 = reader.ReadUInt32(), Unk2 = reader.ReadUInt32(), Unk3 = reader.ReadUInt32() };
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(Unk0);
            writer.Write(Unk1);
            writer.Write(Unk2);
            writer.Write(Unk3);
        }
    }

    public class CompleteSwitchData : IGameData<CompleteSwitchData>
    {
        public uint Unk { get; set; }
        public uint Unk0 { get; set; }

        public static CompleteSwitchData Read(BinaryReader reader)
        {
            return new CompleteSwitchData() { Unk0 = reader.ReadUInt32() };
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(Unk0);
        }
    }

    public class CompleteGraphicsData : IGameData<CompleteGraphicsData>
    {
        public uint Unk { get; set; }
        public uint Unk0 { get; set; }
        public uint Unk1 { get; set; }
        public uint Unk2 { get; set; }

        public static CompleteGraphicsData Read(BinaryReader reader)
        {
            return new CompleteGraphicsData() { Unk0 = reader.ReadUInt32(), Unk1 = reader.ReadUInt32(), Unk2 = reader.ReadUInt32() };
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(Unk0);
            writer.Write(Unk1);
            writer.Write(Unk2);
        }
    }

    public class CompleteMediaData : IGameData<CompleteMediaData>
    {
        public uint Unk { get; set; }
        public uint Unk0 { get; set; }
        public uint Unk1 { get; set; }
        public uint Unk2 { get; set; }

        public static CompleteMediaData Read(BinaryReader reader)
        {
            return new CompleteMediaData() { Unk0 = reader.ReadUInt32(), Unk1 = reader.ReadUInt32(), Unk2 = reader.ReadUInt32() };
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(Unk0);
            writer.Write(Unk1);
            writer.Write(Unk2);
        }
    }

    public class ExecuteRefData : IGameData<ExecuteRefData>
    {
        public uint Unk { get; set; }
        public uint Unk0 { get; set; }
        public uint Unk1 { get; set; }

        public static ExecuteRefData Read(BinaryReader reader)
        {
            return new ExecuteRefData() { Unk0 = reader.ReadUInt32(), Unk1 = reader.ReadUInt32() };
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(Unk0);
            writer.Write(Unk1);
        }
    }

    public class TypeIDData : IGameData<TypeIDData>
    {
        public uint Unk { get; set; }
        public uint Unk0 { get; set; }

        public static TypeIDData Read(BinaryReader reader)
        {
            return new TypeIDData() { Unk0 = reader.ReadUInt32() };
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(Unk0);
        }
    }
}
