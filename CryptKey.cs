﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRPGStudioToolkit
{
    public class CryptKey
    {
        public static string SecretDummy = "_dummy";
        public static string SecretKeyset = "keyset";

        private interface ICryptKeyHandler
        {
            byte[] DecryptBytes(ReadOnlySpan<byte> bytes);
            byte[] EncryptBytes(ReadOnlySpan<byte> bytes);
        }

        private class RC4CryptKeyHandler : ICryptKeyHandler
        {
            private RC4Engine Engine { get; }
            private byte[] HashBytes { get; }

            public RC4CryptKeyHandler(ReadOnlySpan<byte> hashBytes)
            {
                this.Engine = new RC4Engine();
                this.HashBytes = hashBytes.ToArray();
            }

            byte[] ICryptKeyHandler.DecryptBytes(System.ReadOnlySpan<byte> bytes)
            {
                byte[] result = new byte[bytes.Length];
                this.Engine.Init(false, new KeyParameter(this.HashBytes));
                this.Engine.ProcessBytes(bytes, result);
                return result;
            }

            byte[] ICryptKeyHandler.EncryptBytes(System.ReadOnlySpan<byte> bytes)
            {
                byte[] result = new byte[bytes.Length];
                this.Engine.Init(true, new KeyParameter(this.HashBytes));
                this.Engine.ProcessBytes(bytes, result);
                return result;
            }
        }

        private class RC2CryptKeyHandler : ICryptKeyHandler
        {
            private byte[] HashBytes { get; }

            public RC2CryptKeyHandler(ReadOnlySpan<byte> hashBytes)
            {
                this.HashBytes = hashBytes.ToArray();
            }

            byte[] ICryptKeyHandler.DecryptBytes(System.ReadOnlySpan<byte> bytes)
            {
                using var rc2 = System.Security.Cryptography.RC2.Create();
                rc2.Key = this.HashBytes;
                return rc2.DecryptCbc(bytes, new byte[8], System.Security.Cryptography.PaddingMode.Zeros);
            }

            byte[] ICryptKeyHandler.EncryptBytes(System.ReadOnlySpan<byte> bytes)
            {
                using var rc2 = System.Security.Cryptography.RC2.Create();
                rc2.Key = this.HashBytes;
                return rc2.EncryptCbc(bytes, new byte[8], System.Security.Cryptography.PaddingMode.PKCS7);
            }
        }

        public string Secret { get; }
        public int CryptMode { get; }

        private ICryptKeyHandler Handler { get; }

        public CryptKey(string secret, int cryptMode)
        {
            this.Secret = secret;
            this.CryptMode = cryptMode;

            // The secret (as a wstring, or UCS2) is hashed with MD5
            byte[] secretBytes = System.Text.Encoding.Unicode.GetBytes(secret);
            byte[] hashBytes = System.Security.Cryptography.MD5.HashData(secretBytes.AsSpan().Slice(0, secret.Length)); // It looks like the devs mixed up wide-string character count vs byte length, lol

            // CryptDeriveKey
            bool bUseRC4 = cryptMode == -1; // otherwise RC2

            this.Handler = bUseRC4 ? new RC4CryptKeyHandler(hashBytes) : new RC2CryptKeyHandler(hashBytes);
        }

        public byte[] DecryptBytes(ReadOnlySpan<byte> bytes)
        {
            return this.Handler.DecryptBytes(bytes);
        }

        public byte[] EncryptBytes(ReadOnlySpan<byte> bytes)
        {
            return this.Handler.EncryptBytes(bytes);
        }
    }
}
